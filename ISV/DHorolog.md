# $HOROLOG

$H\[OROLOG\] is a read-only comma separated string of two integers, the first is the number of days since December, 31, 1840, and the second is the number of seconds since the midnight (0 through 86399).

## EXAMPLE

```
  Write $HOROLOG
```

Produces the result `58883,55555` at 3:25:55 pm on March 20, 2002.

## DISCUSSION

- The standard does not specify a time zone. It is traditional to use the timezone of the process.
- The standard does not specify how to handle changes in time zone, e.g., transitioning from Daylight Savings to Standard time and vice versa. To avoid confusion, it is recommended that `$HOROLOG` follow the practice of the underlying operating system for processes with that timezone.
- The standard does not specify how to handle leap seconds. To avoid confusing application code with a seconds value of 86400, it is recommended to use an operating system clock configured to slew rather than to step, and if running on an operating system that is configured to step, to hold the number of seconds at 86399 for two seconds.
